from django.shortcuts import render, get_object_or_404, redirect, reverse
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponsePermanentRedirect
from .models import Animation, Image
import django_rq
from rq.job import Job


@login_required
def index(request):
    if request.method == "POST":
        anim = Animation.objects.create(name=request.POST["name"], user=request.user)
        for img in request.FILES.getlist("imgs"):
            Image.objects.create(animation=anim, image=img)
        anim.enqueue({})

    anims = Animation.objects.all()
    context = {"anims": anims}
    return render(request, "mkgif/index.html", context)


@login_required
def details(request, pk):
    anim = get_object_or_404(Animation, pk=pk)

    images = Image.objects.filter(animation=pk)
    context = {"anim": anim, "images": images}
    if request.method == "GET":
        if anim.user == request.user:
            context["is_my_anim"] = True

    elif request.method == "DELETE":
        if anim.user == request.user:
            anim.delete()
            images.delete()
            return redirect("mkgif:index")
        else:
            raise PermissionDenied("This animation does not belong to you!")

    return render(request, "mkgif/details.html", context)
