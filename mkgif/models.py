from django.conf import settings
from django.db import models
from django.contrib.auth.models import User
import django_rq
from .utils import mk_gif_ffmpeg
import os


class Animation(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)

    def enqueue(self, params):
        django_rq.enqueue(
            mk_gif_ffmpeg,
            job_id=f"{self.pk}",
            kwargs={
                "pk": self.pk,
                "params": params,
            },
        )

    @property
    def worker_is_done(self):
        output_exists = os.path.exists(f"media/{self.pk}/out.gif")
        queue = django_rq.get_queue("default")
        job = queue.fetch_job(f"{self.pk}")
        result = job.result if job else None

        if result and output_exists:
            return True
        elif output_exists and job == None:
            return True
        else:
            return False


class Image(models.Model):
    def image_path(self, filename):
        return f"{self.animation.pk}/{filename}"

    animation = models.ForeignKey("Animation", on_delete=models.CASCADE)
    image = models.ImageField(upload_to=image_path)
